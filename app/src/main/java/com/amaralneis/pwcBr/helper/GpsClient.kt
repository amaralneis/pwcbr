package com.amaralneis.pwcBr.helper

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices

class GpsClient(val context: Context,
                val preExecute: () -> Unit = {},
                val finished: () -> Unit = {},
                val updateLocation: (value: Location) -> Unit = {},
                val failure: () -> Unit = {},
                val requestPermission: () -> Unit = {}):
    GoogleApiClient.ConnectionCallbacks, LocationListener {

    private val client: GoogleApiClient = GoogleApiClient.Builder(context)
        .addApi(LocationServices.API)
        .addConnectionCallbacks(this)
        .build()
    private var location: Location? = null


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        val request = LocationRequest()
        request.smallestDisplacement = 50f // Metros
        request.interval = 1000 // a x segundos
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        LocationServices.FusedLocationApi.requestLocationUpdates(client, request, this)

    }

    override fun onConnectionSuspended(p0: Int) {
        failure()
        finished()
    }

    override fun onLocationChanged(location: Location?) {
        this.location = location
        location?.let {
            updateLocation(it)
            finished()
        }
    }

    fun start() {
        if (checkPermisionLocation()) {
            client.connect()
        } else {
            requestPermission()
        }
    }

    fun connect() {
        preExecute()
        client.connect()
    }

    fun disconnect() {
        client.disconnect()
    }

    private fun checkPermisionLocation(): Boolean {
        return (ContextCompat.checkSelfPermission(context,
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
    }

}


package com.amaralneis.pwcBr.helper

import android.content.Context
import android.net.ConnectivityManager

class Utils {

    companion object {
        fun isOnline(): Boolean {

            PwcApplication.appContext?.let {
                val manager =
                    it.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

                val activeNetworkInfo = manager.activeNetworkInfo
                return activeNetworkInfo != null && activeNetworkInfo.isConnected
            }

            return false
        }
    }
}
package com.amaralneis.pwcBr.helper

import android.app.Application
import android.content.Context
import java.lang.ref.WeakReference


class PwcApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        context =
            WeakReference(applicationContext)
    }

    companion object {
        private var context: WeakReference<Context>? = null
        val appContext: Context?
            get() = context!!.get()
    }
}
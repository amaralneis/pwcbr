package com.amaralneis.pwcBr.helper

import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.view.View
import com.amaralneis.myapplication.R
import com.google.android.material.snackbar.Snackbar

class Alerts {

    companion object {

        fun loading(context: Context, title: String = ""): ProgressDialog {

            return ProgressDialog.show(
                context,
                title,
                context.getString(R.string.loading),
                true,
                false
            )
        }

        fun snackInfo(view: View, msg: String) {
            val snackbar: Snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
            snackbar.view
                .setBackgroundColor(view.context.resources.getColor(R.color.mdYellow800))
            snackbar.setActionTextColor(Color.parseColor("#FFFFEE19"))
            snackbar.show()
        }

        fun snackSuccess(view: View, msg: String) {
            val snackbar: Snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
            snackbar.view
                .setBackgroundColor(view.context.resources.getColor(R.color.mdGreen500))
            snackbar.setActionTextColor(Color.parseColor("#FFFFEE19"))
            snackbar.show()
        }

    }
}
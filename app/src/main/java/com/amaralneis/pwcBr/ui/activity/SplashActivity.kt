package com.amaralneis.pwcBr.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.amaralneis.myapplication.R
import com.amaralneis.pwcBr.extension.changeStatusBarColor
import java.util.*

class SplashActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        changeStatusBarColor()
        setSleepScreen()

    }

    private fun setSleepScreen() {
        val task = object : TimerTask() {
            override fun run() {
                val intent = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        Timer().schedule(task, 3000)
    }
}


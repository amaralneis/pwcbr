package com.amaralneis.pwcBr.ui.activity

import android.Manifest
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.amaralneis.myapplication.R
import com.amaralneis.pwcBr.extension.checkConnection
import com.amaralneis.pwcBr.helper.Alerts
import com.amaralneis.pwcBr.helper.GpsClient
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var address: Address? = null
    private val alert by lazy {Alerts.loading(this, "Obtendo localização")}

    private val gpsClient by lazy {
        GpsClient(this,
            preExecute = {},
            finished = {alert.dismiss()},
            updateLocation = {updateLocationWith(it)},
            requestPermission = {requestPermission()})
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults[0] != -1) {
            when (requestCode) {
                468 -> gpsClient.connect()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        alert.show()
        gpsClient.start()
    }

    override fun onResume() {
        super.onResume()
        checkConnection()
    }

    private fun updateLocationWith(location: Location) {
        val geocoder = Geocoder(this)

        val addresses: List<Address>? = geocoder.getFromLocation(location.latitude, location.longitude, 1)
        if (!addresses.isNullOrEmpty()) {
            this.address = addresses[0]
            setupTextView()
        }
    }

    private fun setupTextView() {
        address?.let {
            val addressLine = it.getAddressLine(0)
            val msg = "Clique em iniciar para participar da nossa enquete! Faremos perguntas referente ao endereço $addressLine"
            addressTextView.text = msg



        }

    }


    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION), 468)
    }

    fun startQuestionClick(view: View) {

        checkConnection(success = {
            address?.let {
                val intent = Intent(this, QuestionActivity::class.java)
                intent.putExtra("address", it)
                startActivity(intent)
                return@checkConnection
            }

            Alerts.snackInfo(view, getString(R.string.you_must_have_access_to_your_address_to_continue))

        })


    }


}

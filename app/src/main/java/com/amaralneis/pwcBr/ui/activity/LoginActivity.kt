package com.amaralneis.pwcBr.ui.activity

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.amaralneis.myapplication.R
import com.amaralneis.pwcBr.extension.afterTextChanged
import com.amaralneis.pwcBr.extension.checkConnection
import com.amaralneis.pwcBr.extension.isNotEmailValid
import com.amaralneis.pwcBr.helper.Alerts
import com.amaralneis.pwcBr.network.login.LoginClient
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private var email = ""
    private var password = ""
    private var showPassword = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setupEditText()
    }

    override fun onResume() {
        super.onResume()
        checkConnection()
    }

    private fun setupEditText() {
        emailEditText.afterTextChanged { email = it}
        passwodEditText.afterTextChanged { password = it }
    }

    private fun valid(view: View): Boolean {
        if (email.isEmpty()) {
            Alerts.snackInfo(view, "Informe o seu e-mail")
            return false
        }

        if (email.isNotEmailValid()) {
            Alerts.snackInfo(view, "Informe um e-mail válido")
            return false
        }

        if (password.isEmpty()) {
            Alerts.snackInfo(view, "Informe sua senha")
            return false
        }

        return true
    }

    private fun getTypeLabelPassword() = if (showPassword) {
        passwodEditText.setTransformationMethod(null)
    } else passwodEditText.setTransformationMethod(PasswordTransformationMethod.getInstance())


    private fun getDrawbleShowPassword(): Drawable? = if (showPassword) {
        getDrawable(R.drawable.ic_visibility)
    } else getDrawable(R.drawable.ic_visibility_off)

    private fun login() {
        val client = LoginClient()
        val dialog = Alerts.loading(this)
        client.loginWith(email, password,
            preExecute = {dialog.show()},
            finished =  {dialog.dismiss()},
            failure = {failureLogin(it)},
            success = {mainApp()}
        )
    }

    private fun mainApp() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun failureLogin(error: String) {
        Alerts.snackInfo(passwodEditText, error)
    }

    fun loginClick(view: View) {
        checkConnection( success = {
            if (valid(view)) {
                login()
            }
        })
    }

    fun showPasswordClick(view: View) {
        showPassword = !showPassword
        showPasswordImageView.setImageDrawable(getDrawbleShowPassword())
        getTypeLabelPassword()

    }



}
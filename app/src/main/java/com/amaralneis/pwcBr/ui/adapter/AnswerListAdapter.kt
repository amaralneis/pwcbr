package com.amaralneis.pwcBr.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.amaralneis.myapplication.R
import kotlinx.android.synthetic.main.row_answer.view.*


class AnswerListAdapter(
    private val context: Context,
    private val itens: List<String>,
    private val next: () -> Unit = {}) : RecyclerView.Adapter<AnswerListAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.row_answer, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = itens.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(itens[position],  next = { next()})
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindView(item: String, next: () -> Unit = {}) {
            itemView.answerTextView.text = item
            itemView.answerTextView.setOnClickListener {
                itemView.answerTextView.setTextColor(ContextCompat.getColor(it.context, R.color.white))
                it.setBackgroundColor(ContextCompat.getColor(it.context, R.color.colorPrimary))
                next()
            }
        }
    }
}
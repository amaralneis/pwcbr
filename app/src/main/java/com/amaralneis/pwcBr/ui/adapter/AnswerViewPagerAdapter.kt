package com.amaralneis.pwcBr.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.amaralneis.pwcBr.model.Survey
import kotlinx.android.synthetic.main.question_page_view.view.*

class AnswerViewPagerAdapter(private val list: List<View>,
                             private val polls: List<Survey>,
                             private val next: (position: Int) -> Unit = {}): PagerAdapter() {

    override fun isViewFromObject(view: View, objectAny: Any): Boolean = view == objectAny

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) = container.removeView(any as View)

    override fun getItemPosition(`object`: Any): Int = POSITION_NONE

    override fun getCount(): Int = list.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = list[position]
        setupViewWith(view, position, polls[position], next = { next(it)})
        container.addView(view)
        return view
    }

    private fun setupViewWith(view: View, position: Int, survey: Survey, next: (position: Int) -> Unit = {}) {

        view.surveyTitleTextView.text = survey.question
        with(view.surveyRecyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = AnswerListAdapter(view.context, survey.answers, next = { next(position) })
        }
    }
}
package com.amaralneis.pwcBr.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.amaralneis.myapplication.R

class OffilineActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.activity_offline)
    }

    fun clickClose(view: View?) {
        finish()
    }

    fun clickTryAgain(view: View?) {
        finish()
    }
}
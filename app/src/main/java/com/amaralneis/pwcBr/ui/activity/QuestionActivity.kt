package com.amaralneis.pwcBr.ui.activity

import android.location.Address
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.amaralneis.myapplication.R
import com.amaralneis.pwcBr.extension.changeStatusBarColor
import com.amaralneis.pwcBr.model.Survey
import com.amaralneis.pwcBr.ui.adapter.AnswerViewPagerAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_question.*
import kotlinx.android.synthetic.main.bottom_sheet_view.view.*


class QuestionActivity: AppCompatActivity() {

    private var polls: MutableList<Survey> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        changeStatusBarColor(R.color.surveyStatusBar)
        setupPolls()
        setupPageView()
    }

    private fun setupPolls() {
        val address = intent.getParcelableExtra<Address>("address")
        polls = Survey.listTempWith(address)
    }


    private fun  setupPageView() {
        viewPagerQuestion.adapter = AnswerViewPagerAdapter(getListView(), polls, next = {
            Handler().postDelayed({ setupNextPage(it + 1) }, 500)

        })

        pageIndicatorView.setViewPager(viewPagerQuestion)
    }

    private fun getListView(): List<View> {
        val inflater = LayoutInflater.from(this)
        val listView: MutableList<View> = mutableListOf()
        repeat(polls.size) { listView.add(inflater.inflate(R.layout.question_page_view, null)) }

        return listView

    }

    private fun setupNextPage(position: Int) {


        if (position < polls.size) {
            viewPagerQuestion.currentItem = position
            return
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        val sheetView: View = layoutInflater.inflate(R.layout.bottom_sheet_view, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()

        sheetView.sheetButton.setOnClickListener { finishClick(it) }

    }


    fun finishClick(view: View) {
        finish()
    }
}
package com.amaralneis.pwcBr.model

import android.location.Address

class Survey(val question: String, val answers:List<String>) {


    companion object {
        fun listTempWith(address: Address): MutableList<Survey> {

            val polls = mutableListOf<Survey>()

            val city = "${address.subAdminArea} - ${address.adminArea}"

            val question1 = "Qual sua opinião sobre o turismo na cidade de $city?"
            val question2 = "Qual sua opinião sobre a segurança na cidade de $city?"

            polls.add(Survey(question1, answersFirstQuestion()))
            polls.add(Survey(question2, answersSecondQuestion()))

            return polls

        }

       private fun answersFirstQuestion() = mutableListOf("Bom", "Médio", "Ruim")
       private fun answersSecondQuestion() = mutableListOf("Muito boa", "Boa", "Regular", "Ruim", "Muito  ruim")
    }
}
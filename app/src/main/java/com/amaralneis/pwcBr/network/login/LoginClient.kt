package com.amaralneis.pwcBr.network.login

import android.os.Handler

class LoginClient {

    fun loginWith(email: String,
                  password: String,
                  preExecute: () -> Unit = {},
                  finished: () -> Unit = {},
                  success: (value: String) -> Unit = {},
                  failure: (error: String) -> Unit = {} ) {

        preExecute()
        Handler().postDelayed({
            if (email == "test@abacomm.com.br" && password == "1234") {
                success("Login realizado com sucesso!")
            } else {
                failure("Verifique seu usuário e senha")
            }

            finished()
        }, 4000)
    }
}
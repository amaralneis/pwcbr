package com.amaralneis.pwcBr.extension

fun String.isEmailValid() = android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
fun String.isNotEmailValid() = !isEmailValid()
package com.amaralneis.pwcBr.extension

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.amaralneis.myapplication.R
import com.amaralneis.pwcBr.helper.Utils
import com.amaralneis.pwcBr.ui.activity.OffilineActivity

fun AppCompatActivity.changeStatusBarColor (color: Int = R.color.colorPrimary) {
    val color = ContextCompat.getColor(this, color)
    window.statusBarColor = color
}

fun AppCompatActivity.checkConnection(success: () -> Unit = {}) {

    if (Utils.isOnline()) {
        success()
    } else {
        startActivity(Intent(this, OffilineActivity::class.java))
    }
}
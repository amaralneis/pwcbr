# PwC BR
O PwC BR é um aplicativo que realiza enqetes, sobre sua cidade atual.
### Executando 
  ``` Este projeto foi desenvolvido utilizando android studio 3.5.2 e Kotlin 1.3.60 ``` 
### Bibliotecas 
  - ``PageIndicatorView`` -> É uma biblioteca leve para indicar a página selecionada do ViewPager com diferentes animações e capacidade de personalizá-lo conforme necessário. Para conhecer um pouco mais clique [aqui](https://github.com/romandanylyk/PageIndicatorView) 
  - `Google Play Services Location` -> Biblioteca utilizada para obter a localização atual do usuário. Para conhecer um pouco mais clique [aqui](https://developers.google.com/android/guides/setup) 

### Usando o app.
Para realizar o login é necessario utilizar o e-mail ``test@abacomm.com.br`` e a senha `1234`. 

### Proximos passos.
	- Criar tela para redefinir a senha.
	- Criar tela para se cadastrar.
	- Implementar comunicação com back-end
	- Salvar usuário logado.
	- Implementar camada de testes.
 